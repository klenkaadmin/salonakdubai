﻿
Partial Class _loader
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim ret As String = ""
        Try

            Dim htmlStringWriter As New System.IO.StringWriter()
            Server.Execute(Request.QueryString("url"), htmlStringWriter)
            Dim htmlOutput = htmlStringWriter.GetStringBuilder().ToString()
            Dim tops As String = midReturn("<!--headstart-->", "<!--headend-->", htmlOutput)
            Dim html As String = midReturn("<!--htmlstart-->", "<!--htmlend-->", htmlOutput)
            Dim js As String = midReturn("//jsstart", "//jsend", htmlOutput)
            ' js = js.Replace("$(", "$get(")
            ret = "<script>" & vbCrLf & js + "</script>" & vbCrLf
            ret = tops & vbCrLf & html & vbCrLf & ret

        Catch ex As Exception
            ret = "error:" & ex.Message & "<br/>URL: <a href='" & Request.QueryString("url") & "' target='_blank'>" & Request.QueryString("url") & "</a>"
        End Try

        clsHelper.FlushResponseGZIP(ret, "text/HTML")

    End Sub

    Public Function midReturn(ByVal first As String, ByVal last As String, ByVal total As String) As String
        Dim ret As String = ""
        If last.Length < 1 Then
            ret = total.Substring(total.IndexOf(first))
        End If
        If first.Length < 1 Then
            ret = total.Substring(0, (total.IndexOf(last)))
        End If
        Try
            ret = ((total.Substring(total.IndexOf(first), (total.IndexOf(last) - total.IndexOf(first)))).Replace(first, "")).Replace(last, "")
        Catch ArgumentOutOfRangeException As Exception
        End Try

        Return ret
    End Function

End Class
