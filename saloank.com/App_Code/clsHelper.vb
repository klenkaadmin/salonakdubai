﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports System.Net.Mail
Imports System
Imports System.Web
Imports System.Configuration
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports System.Net
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports System.Globalization
Imports Newtonsoft.Json
Imports System.IO.Compression
Imports Newtonsoft.Json.Linq
Imports System.Dynamic

Public Class clsHelper

#Region "Controls Area"

    Public Shared Sub PageLoadControls(controls As ControlCollection, dr As DataRow)
        For Each cont As Control In controls
            If TypeOf cont Is HtmlControl Then
                Dim htmlcontrol As HtmlControl = CType(cont, HtmlControl)
                If Not IsNothing(htmlcontrol.Attributes("data-base")) Then
                    Dim rel As String = htmlcontrol.Attributes("data-base").ToLower.ToString
                    If TypeOf htmlcontrol Is HtmlTextArea Then
                        CType(htmlcontrol, HtmlTextArea).Value = dr(rel).ToString
                    ElseIf TypeOf htmlcontrol Is HtmlInputCheckBox Then
                        CType(htmlcontrol, HtmlInputCheckBox).Value = dr(rel).ToString
                        CType(htmlcontrol, HtmlInputCheckBox).Checked = dr(rel).ToString
                    ElseIf TypeOf htmlcontrol Is HtmlInputHidden Then
                        CType(htmlcontrol, HtmlInputHidden).Value = dr(rel).ToString
                    ElseIf TypeOf htmlcontrol Is HtmlInputText Then
                        If CType(htmlcontrol, HtmlInputText).ID.ToString.IndexOf("date") > -1 Then
                            If CDate(dr(rel).ToString) > CDate("1900-1-1") Then
                                CType(htmlcontrol, HtmlInputText).Value = dr(rel).ToString
                            End If
                        Else
                            CType(htmlcontrol, HtmlInputText).Value = dr(rel).ToString
                        End If
                    ElseIf TypeOf htmlcontrol Is HtmlSelect Then
                        If IsNothing(htmlcontrol.Attributes("multiple")) Then
                            CType(htmlcontrol, HtmlSelect).Value = dr(rel).ToString
                        Else
                            Dim vArr() As String = dr(rel).ToString.Split(",")
                            For Each item As ListItem In CType(htmlcontrol, HtmlSelect).Items
                                For Each v As String In vArr
                                    If v.ToString.Trim.ToLower = item.Value.ToString.Trim.ToLower Then
                                        item.Selected = True
                                    End If
                                Next

                            Next
                        End If
                    ElseIf TypeOf htmlcontrol Is HtmlImage Then
                        If dr(rel).ToString.Trim.Length > 0 Then
                            CType(htmlcontrol, HtmlImage).Src = dr(rel).ToString
                        End If
                    Else
                        CType(htmlcontrol, HtmlGenericControl).InnerHtml = dr(rel).ToString
                    End If
                End If
            End If
            If cont.Controls.Count > 0 Then
                PageLoadControls(cont.Controls, dr)
            End If
        Next
    End Sub

#End Region

#Region "Compress and Decompress"
    Public Shared Function CompressString(s As String) As String
        Dim bytes = Encoding.Unicode.GetBytes(s)
        Using msi = New MemoryStream(bytes)
            Using mso = New MemoryStream()
                Using gs = New GZipStream(mso, CompressionMode.Compress)
                    msi.CopyTo(gs)
                End Using
                Return Convert.ToBase64String(mso.ToArray())
            End Using
        End Using
    End Function

    Public Shared Function DecompressString(s As String) As String
        Dim bytes = Convert.FromBase64String(s)
        Using msi = New MemoryStream(bytes)
            Using mso = New MemoryStream()
                Using gs = New GZipStream(msi, CompressionMode.Decompress)
                    gs.CopyTo(mso)
                End Using
                Return Encoding.Unicode.GetString(mso.ToArray())
            End Using
        End Using
    End Function
#End Region

#Region "Encrypt & Decrypt"
    Public Shared Function AES_Encrypt(ByVal input As String) As String
        Dim pass As String = "ITw0r7|"
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim encrypted As String = ""
        Dim hash(31) As Byte
        Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
        Array.Copy(temp, 0, hash, 0, 16)
        Array.Copy(temp, 0, hash, 15, 16)
        AES.Key = hash
        AES.Mode = System.Security.Cryptography.CipherMode.ECB
        Dim DESEncrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateEncryptor
        Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(input)
        encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
        Return encrypted
    End Function

    Public Shared Function AES_Decrypt(ByVal input As String) As String
        Dim pass As String = "ITw0r7|"
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim decrypted As String = ""
        Dim hash(31) As Byte
        Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
        Array.Copy(temp, 0, hash, 0, 16)
        Array.Copy(temp, 0, hash, 15, 16)
        AES.Key = hash
        AES.Mode = System.Security.Cryptography.CipherMode.ECB
        Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor

        Dim Buffer As Byte() = Convert.FromBase64String(input)
        decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
        Return decrypted
    End Function
#End Region

#Region "Response and Request Area"
    Public Shared Function RequestGet(url As String) As StreamReader
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse
        Dim stream As StreamReader
        request = HttpWebRequest.Create(url)
        response = request.GetResponse()
        request.Method = "GET"
        stream = New StreamReader(response.GetResponseStream())
        Return stream
    End Function
    Public Shared Function RequestValueFix(val As Object, def_val As Object) As Object
        If IsNothing(val) Then
            Return def_val
        ElseIf val.ToString.Trim.Length = 0 And val.GetType = GetType(String) Then
            Return def_val
        Else
            Return val
        End If
    End Function
    Public Shared Function RequestCreateObject(obj As Object) As Object
        For Each prop As PropertyInfo In obj.GetType.GetProperties
            Dim propName As String = prop.Name
            Dim propType As Object = prop.PropertyType
            If Not IsNothing(HttpContext.Current.Request(propName)) Then
                Dim val = StringToType(HttpContext.Current.Request(propName), propType)
                prop.SetValue(obj, val, Nothing)
            End If
        Next
        Return obj
    End Function
    Private Shared Function StringToType(value As String, propertyType As Type) As Object
        Dim underlyingType = Nullable.GetUnderlyingType(propertyType)
        If value.ToLower = "null".ToLower Or value.ToLower = "undefined".ToLower Then
            Return Nothing
        ElseIf underlyingType Is Nothing Then
            Return Convert.ChangeType(value, propertyType, CultureInfo.InvariantCulture)
        ElseIf underlyingType.FullName.IndexOf("Boolean") > -1 Then
            Return CBool(value)
        ElseIf underlyingType.FullName.IndexOf("Guid") > -1 Then
            Return (New Guid(value))
        End If
        Return If([String].IsNullOrEmpty(value), Nothing, Convert.ChangeType(value, underlyingType, CultureInfo.InvariantCulture))
    End Function
#End Region


#Region "E-Mail area"
    Public Shared Sub EMailSend(ByVal message As String, ByVal fromMail As String, ByVal tomail As String, ByVal subject As String)
        For Each itm As String In tomail.Split(",")
            If itm.Trim.Length > 5 Then
                Dim smtp As New SmtpClient("127.0.0.1")
                Dim mail As New MailMessage
                mail.Body = message
                mail.From = New MailAddress(fromMail)
                mail.Subject = subject
                mail.To.Add(itm)
                mail.IsBodyHtml = True
                smtp.Send(mail)
            End If
        Next
    End Sub
#End Region

#Region "HTML area"
    Public Shared Function HtmlStrip(ByVal html As String) As String
        Return Regex.Replace(html, "<.*?>", "")
    End Function
#End Region

#Region "Web Config Area"
    Public Shared Function ConfigGet(ByVal key As String) As String
        Return ConfigurationManager.AppSettings(key).ToString()
    End Function
#End Region

#Region "Data Type datatable,int,string,...."
    Public Shared Function CheckJson(str As String) As Boolean
        Try
            Dim tmpObj = JContainer.Parse(str)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Shared Function ArrayMatch(String1 As String, string2 As String) As Boolean
        For Each s2 As String In string2.Split(",")
            If Array.IndexOf(String1.Split(","), s2) > -1 Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Shared Function StringBase64Decoding(stringBase64 As String) As String
        Dim b As Byte() = Convert.FromBase64String(stringBase64)
        Return System.Text.Encoding.UTF8.GetString(b)
    End Function
    Public Shared Function StringBase64Encoding(str As String) As String
        Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(str)
        Return Convert.ToBase64String(byt)
    End Function
    Public Shared Function DataTableFilterByDataView(dt As DataTable, str As String) As DataTable
        Dim dv As DataView = dt.DefaultView
        dv.RowFilter = str
        Return dv.ToTable
    End Function
#End Region

#Region "Json Work Area"

    Public Shared Function JsonDataTablePlugin(ByVal dt As DataTable, ByVal echo As Integer) As String
        Dim count As Integer = 0
        If dt.Rows.Count > 0 Then
            count = dt.Rows(0)("totalcount")
        End If
        Dim obj As Object = New ExpandoObject
        obj.sEcho = echo
        obj.iTotalRecords = count
        obj.iTotalDisplayRecords = count
        obj.aaData = dt
        Return JsonConvert.SerializeObject(obj)
    End Function

    Public Shared Function JsonFull(ByVal dt As DataTable) As String
        Return JsonConvert.SerializeObject(dt)
    End Function
    Public Shared Function JsonFormatFriendly(ByVal s As String) As String
        s = s.Replace("\", "\\")
        s = s.Replace("""", "\""").Replace(vbCr & vbLf, "\n").Replace(vbCr, "\n").Replace(vbLf, "\n").Replace(vbTab, " ").Replace(vbCrLf, "\n")
        's = HtmlStrip(s)
        s = s.Trim
        Return s
    End Function
#End Region

#Region "Flush Work Area"

    Public Shared Sub FlushFileByPath(path As String, Optional Caption As String = "")
        If Caption = "" Then Caption = IO.File.GetAttributes("name").ToString
        Dim array As Byte() = File.ReadAllBytes(path)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.ContentType = "application/x-newton-compatible-pkg"
        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" & Caption)
        HttpContext.Current.Response.BinaryWrite(array)
        HttpContext.Current.Response.End()
    End Sub

    Public Shared Sub FlushResponse(ByVal ret As String, Optional ContentType As String = "text/html;  encoding=utf-8;")
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.ContentType = ContentType
        Dim b As Byte() = HttpContext.Current.Response.ContentEncoding.GetBytes(ret)
        HttpContext.Current.Response.AddHeader("Content-Length", b.Length.ToString)
        HttpContext.Current.Response.Write(ret.ToString)
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
        HttpContext.Current.Response.Close()
    End Sub
    Public Shared Sub FlushResponseAPI(ByVal ResPret As String, Optional ContentType As String = "text/html;  encoding=utf-8;")
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Filter = New System.IO.Compression.GZipStream(HttpContext.Current.Response.Filter, System.IO.Compression.CompressionMode.Compress)
        HttpContext.Current.Response.ContentType = ContentType
        HttpContext.Current.Response.AppendHeader("Content-Encoding", "gzip")
        HttpContext.Current.Response.ContentEncoding = Encoding.Unicode
        Dim buffer = Encoding.UTF8.GetBytes(ResPret)
        HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length)
        HttpContext.Current.Response.End()
        HttpContext.Current.Response.Close()
    End Sub

    Public Shared Sub FlushResponseGZIP(ByVal ResPret As String, Optional ByVal outputtype As String = "text/html", Optional ByVal doZip As Boolean = False)
        '//http://stackoverflow.com/questions/10639337/gzip-compression-asp-net-c-sharp
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.Filter = New System.IO.Compression.GZipStream(HttpContext.Current.Response.Filter, System.IO.Compression.CompressionMode.Compress)
        HttpContext.Current.Response.ContentType = "text/javascript; charset=UTF-8"
        HttpContext.Current.Response.AppendHeader("Content-Encoding", "gzip")
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*")
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Content-Range, Content-Disposition, Content-Description")
        HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000")
        HttpContext.Current.Response.AddHeader("Origin", "*")
        HttpContext.Current.Response.ContentEncoding = Encoding.Unicode
        Dim buffer = Encoding.UTF8.GetBytes(ResPret)
        HttpContext.Current.Response.OutputStream.Write(buffer, 0, buffer.Length)
        HttpContext.Current.Response.End()
        HttpContext.Current.Response.Close()
    End Sub
    Public Shared Sub FlushObjectasJson(ByVal obj As Object)
        FlushResponse(JsonConvert.SerializeObject(obj))
    End Sub

    Public Shared Sub FlushResponseImage(ByVal MS As MemoryStream)
        HttpContext.Current.Response.ClearContent()
        HttpContext.Current.Response.ContentType = "image/png"
        HttpContext.Current.Response.BinaryWrite(MS.ToArray())
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Public)
        HttpContext.Current.Response.Cache.SetMaxAge(New TimeSpan(1, 0, 0))
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.End()
        HttpContext.Current.Response.Close()
    End Sub


#End Region

#Region "XML Area"
    Public Shared Function XmlFull(ByVal dt As DataTable) As String
        Dim str As New MemoryStream()
        dt.WriteXml(str, True)
        str.Seek(0, SeekOrigin.Begin)
        Dim sr As New StreamReader(str)
        Dim xmlstr As String
        xmlstr = sr.ReadToEnd()
        Return (xmlstr)
    End Function
#End Region

#Region "Cookie"

    Public Shared Sub CookieSet(CookieName As String, value As String)
        Dim myCookie As HttpCookie = New HttpCookie(CookieName)
        myCookie.Value = AES_Encrypt(value)
        myCookie.Expires = Now.AddDays(2)
        HttpContext.Current.Response.Cookies.Add(myCookie)
    End Sub
    Public Shared Sub CookieDelete(CookieName As String)
        Dim myCookie As HttpCookie
        myCookie = New HttpCookie(CookieName)
        myCookie.Expires = DateTime.Now.AddDays(-1)
        myCookie.Value = ""
        HttpContext.Current.Response.Cookies.Add(myCookie)
        HttpContext.Current.Request.Cookies.Remove(CookieName)
    End Sub
    Public Shared Function CookieGet(CookieName As String) As String
        Try
            Return AES_Decrypt(HttpContext.Current.Request.Cookies(CookieName).Value.ToString)
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("/login.aspx")
            Return Nothing
        End Try
    End Function

#End Region

End Class
