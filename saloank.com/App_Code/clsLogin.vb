﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports Newtonsoft.Json

Public Class clsLogin
    Public Shared ReadOnly Property Role_ID As Integer
        Get
            Return 1 'getCookie("Role_ID")
        End Get
    End Property
    Public Shared ReadOnly Property Agency_ID As Integer
        Get
            Return 1 ' getCookie("Agency_ID")
        End Get
    End Property
    Public Shared ReadOnly Property ID As Integer
        Get
            Return 1 'getCookie("User_ID")
        End Get
    End Property
    Public Shared ReadOnly Property Name As String
        Get
            Return "Mahmoud fawzy" 'getCookie("User_Name")
        End Get
    End Property
    Public Shared ReadOnly Property SiteLanguage As String
        Get
            Return "ar" 'getCookie("Languages")
        End Get
    End Property

    Public Shared Sub SetLoginCookie(dr As DataRow)
        Dim dyn As Object = New System.Dynamic.ExpandoObject()
        dyn.Role_ID = dr("Role_ID").ToString()
        dyn.Website_ID = dr("Website_ID").ToString()
        dyn.User_ID = dr("ID").ToString()
        dyn.User_Name = dr("Name").ToString()
        dyn.Languages = dr("Languages").ToString()
        dyn.User_JobTitle = dr("JobTitle").ToString()
        Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(dyn))
        Dim data As String = JsonConvert.SerializeObject(dyn)
        FormsAuthentication.SetAuthCookie(data, False)


        'Dim key_ftp As Object = JsonConvert.DeserializeObject(Of Dynamic.ExpandoObject)(clsHelper.AES_Decrypt(clsHelper.CookieGet("FTP_Key")))

        Dim dyn_ftp As Object = New System.Dynamic.ExpandoObject()
        dyn_ftp.FTPServer = dr("FTPServer").ToString()
        dyn_ftp.FTPUserName = dr("FTPUserName").ToString()
        dyn_ftp.FTPPassword = dr("FTPPassword").ToString()
        dyn_ftp.FTPPath = dr("FTPPath").ToString()
        dyn_ftp.FTPFilePath = dr("FilePath").ToString()
        clsHelper.CookieSet("FTP_Key", clsHelper.AES_Encrypt(JsonConvert.SerializeObject(dyn_ftp)))
    End Sub

    Public Shared Function getCookie() As DataRow
        Try
            Dim data As String = ""
            If System.Web.HttpContext.Current.User.Identity.IsAuthenticated Then
                Dim authCookie As HttpCookie = HttpContext.Current.Request.Cookies(FormsAuthentication.FormsCookieName)
                Dim ticketas As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)
                data = ticketas.Name
            Else
                data = clsHelper.AES_Decrypt(HttpContext.Current.Request("APIKey"))
            End If
            Dim table As DataTable = JsonConvert.DeserializeObject(Of DataTable)("[" & data & "]")
            Return table.Rows(0)
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("/Login.aspx?ReturnUrl=" & HttpContext.Current.Request.Url.AbsolutePath)
            Return (New DataTable).Rows(0)
        End Try
    End Function
    Public Shared Function getCookie(key As String) As String
        Return getCookie()(key).ToString
    End Function
End Class
