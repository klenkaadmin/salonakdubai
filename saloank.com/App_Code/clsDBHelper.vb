﻿Imports System.Data.SqlClient
Imports System.Data

Public Class clsDBHelper
    Private Shared ReadOnly Property ConnectionString() As String
        Get
            Return clsHelper.ConfigGet("ConnectionString")
        End Get
    End Property


#Region "Command"
    Public Shared Function GetCommand(ConnectionString As String) As SqlCommand
        Dim cmd As New SqlCommand
        cmd.CommandTimeout = 300
        Dim myconn As New SqlConnection(ConnectionString)
        cmd.Connection = myconn
        Return cmd
    End Function
    Public Shared Function GetCommand() As SqlCommand
        Return GetCommand(ConnectionString)
    End Function
    Public Shared Function GetCommand(ConnectionString As String, CommandType As CommandType) As SqlCommand
        Dim cmd As SqlCommand = GetCommand(ConnectionString)
        cmd.CommandType = CommandType
        Return cmd
    End Function
    Public Shared Function GetCommand(ConnectionString As String, CommandType As CommandType, CommandText As String) As SqlCommand
        Dim cmd As SqlCommand = GetCommand(ConnectionString)
        cmd.CommandType = CommandType
        cmd.CommandText = CommandText
        Return cmd
    End Function
    Public Shared Function GetCommand(CommandType As CommandType, CommandText As String) As SqlCommand
        Dim cmd As SqlCommand = GetCommand(ConnectionString)
        cmd.CommandType = CommandType
        cmd.CommandText = CommandText
        Return cmd
    End Function

    Public Shared Function GetCommand(CommandType As CommandType) As SqlCommand
        Return GetCommand(ConnectionString, CommandType)
    End Function
#End Region

#Region "Execute"

    Public Shared Function ExecuteDataReader(ByVal cmd As SqlCommand) As SqlDataReader
        Return cmd.ExecuteReader
    End Function
    Public Shared Function ExecuteDataSet(ByVal cmd As SqlCommand) As DataSet
        Dim da As New SqlDataAdapter
        da.SelectCommand = cmd
        Dim ds As New DataSet
        da.Fill(ds)
        Return ds
    End Function
    Public Shared Function ExecuteDataTable(ByVal cmd As SqlCommand) As DataTable
        Dim ds As DataSet = ExecuteDataSet(cmd)
        Return ds.Tables(ds.Tables.Count - 1)
    End Function
    Public Shared Function ExecuteDataTable(ByVal type As CommandType, query As String) As DataTable
        Dim cmd As SqlCommand = GetCommand(type)
        cmd.CommandType = type
        cmd.CommandText = query
        Return ExecuteDataTable(cmd)
    End Function
    Public Shared Function ExecuteDataRow(ByVal cmd As SqlCommand) As DataRow
        Return ExecuteDataTable(cmd).Rows(0)
    End Function
    Public Shared Function ExecuteDataRow(ByVal type As CommandType, query As String) As DataRow
        Dim cmd As SqlCommand = GetCommand(type)
        cmd.CommandType = type
        cmd.CommandText = query
        Return ExecuteDataRow(cmd)
    End Function
    Public Shared Function ExecuteScalar(ByVal cmd As SqlCommand) As String
        cmd.Connection.Open()
        Dim ret = cmd.ExecuteScalar()
        cmd.Connection.Close()
        Return ret
    End Function
    Public Shared Sub ExecuteNonQuery(ByVal cmd As SqlCommand)
        cmd.Connection.Open()
        Dim ret = cmd.ExecuteNonQuery()
        cmd.Connection.Close()
    End Sub
    Public Shared Sub ExecuteNonQuery(ByVal type As CommandType, query As String)
        Dim cmd As SqlCommand = GetCommand(type)
        cmd.CommandType = type
        cmd.CommandText = query
        ExecuteNonQuery(cmd)
    End Sub
#End Region
End Class
