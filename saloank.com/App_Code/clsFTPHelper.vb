﻿Imports System.IO
Imports System.Net.FtpClient
Imports System.Net
Imports System.Data
Imports Newtonsoft.Json
Imports System.Web

Public Class clsFTPHelper
    Public Property FTP_Server As String = ""
    Public Property FTP_User As String = ""
    Public Property FTP_Password As String = ""
    Public Property FTP_DefaultPath As String = ""

    Public Sub New(_FTP_Server As String, _FTP_User As String, _FTP_Password As String, _FTP_DefaultPath As String)
        FTP_Server = _FTP_Server
        FTP_User = _FTP_User
        FTP_Password = _FTP_Password
        FTP_DefaultPath = _FTP_DefaultPath
    End Sub

#Region "Folder"
    Public Function FolderContent(Path As String) As DataTable
        Using ftp = New FtpClient()
            Dim dt As New DataTable()
            dt.Columns.Add("Created")
            dt.Columns.Add("FullName")
            dt.Columns.Add("Modified")
            dt.Columns.Add("Name")
            dt.Columns.Add("Size")
            dt.Columns.Add("Type")
            dt.Columns.Add("Order")
            dt.Columns.Add("Icon")
            dt.Columns.Add("Path")

            ftp.Host = FTP_Server
            ftp.Credentials = New NetworkCredential(FTP_User, FTP_Password)
            ftp.SetWorkingDirectory(FTP_DefaultPath & Path)
            For Each item In ftp.GetListing(ftp.GetWorkingDirectory())

                Dim dr As DataRow = dt.NewRow
                Select Case item.Type
                    Case FtpFileSystemObjectType.Directory
                        dr("Created") = item.Created
                        dr("FullName") = Path & "/" & item.Name
                        dr("Modified") = item.Modified
                        dr("Name") = item.Name
                        dr("Size") = item.Size
                        dr("Type") = item.Type
                        dr("Order") = "1"
                        dr("Icon") = "folder"
                        dr("Path") = Path
                    Case FtpFileSystemObjectType.File
                        dr("Created") = item.Created
                        dr("FullName") = Path & "/" & item.Name
                        dr("Modified") = item.Modified
                        dr("Name") = item.Name
                        dr("Size") = item.Size
                        dr("Type") = item.Type
                        dr("Order") = "2"
                        dr("Icon") = System.IO.Path.GetExtension(item.FullName).Replace(".", "")
                        dr("Path") = Path
                End Select
                dt.Rows.Add(dr)
            Next

            Dim dataView As New DataView(dt)
            dataView.Sort = " Order asc, Name asc"
            Return dataView.ToTable()
        End Using
    End Function
    Public Sub FolderCreate(Path As String)
        Using conn As New FtpClient()
            conn.Host = FTP_Server
            conn.Credentials = New NetworkCredential(FTP_User, FTP_Password)
            conn.CreateDirectory(FTP_DefaultPath & Path, True)
        End Using
    End Sub
    Public Sub FolderDelete(Path As String)
        Using conn As New FtpClient()
            conn.Host = FTP_Server
            conn.Credentials = New NetworkCredential(FTP_User, FTP_Password)
            conn.DeleteDirectory(FTP_DefaultPath & Path, True, FtpListOption.AllFiles Or FtpListOption.ForceList)
        End Using
    End Sub

#End Region

#Region "File"
    Public Sub FileDelete(Path As String)
        Using conn As New FtpClient()
            conn.Host = FTP_Server
            conn.Credentials = New NetworkCredential(FTP_User, FTP_Password)
            conn.DeleteFile(FTP_DefaultPath & Path)
        End Using
    End Sub
    Public Sub FileUpload(Path As String, File As HttpPostedFile)
        Dim FullName As String = "ftp://" & FTP_Server & FTP_DefaultPath & Path & "/" & File.FileName
        Dim request As FtpWebRequest = DirectCast(FtpWebRequest.Create(FullName), FtpWebRequest)

        request.Method = WebRequestMethods.Ftp.UploadFile
        request.Credentials = New NetworkCredential(FTP_User, FTP_Password)
        request.UsePassive = True
        request.UseBinary = True
        request.KeepAlive = False
        Using reqStream As Stream = request.GetRequestStream()
            Dim fileData As Byte() = Nothing
            Using binaryReader = New BinaryReader(File.InputStream)
                fileData = binaryReader.ReadBytes(File.ContentLength)
            End Using
            reqStream.Write(fileData, 0, fileData.Length)
        End Using
    End Sub
    Public Sub FileUpload(Path As String, Files As HttpFileCollection)
        For i As Integer = 0 To Files.Count - 1
            FileUpload(Path, Files(i))
        Next
    End Sub
#End Region

End Class
