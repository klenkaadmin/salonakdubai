﻿
Imports System.Data
Imports System.Globalization
Imports System.IO
Imports System.Reflection
Imports System.Xml
Imports Newtonsoft.Json

Partial Class API_Default
    Inherits System.Web.UI.Page

    Private Sub API_Default_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim Target As String = Request("Target").Trim.ToLower
        Dim Action As String = Request("Action").Trim.ToLower
        If Target = "" Then
            FlushApiResponse("No Target")
        Else
            '===================================================== fun_
            If Target = "Users".ToLower Then
                If Action = "get".ToLower Then
                    FlushApiResponse(Users_Get())
                End If
            End If
        End If

    End Sub

#Region "Users"
    Function Users_Get() As String
        Return "{""name"":""ahmed""}"
    End Function
#End Region
#Region "General"
    'Function Switch_Website() As String
    '    Dim obj_web As New clsWebsite
    '    With obj_web
    '        .ID = Request("Website_ID")
    '        .Login_ID = clsMethod.User_ID
    '    End With
    '    Dim dr As DataRow = clsWebsite.Switch(obj_web)

    '    clsMethod.SetLoginCookie(dr)

    '    Return clsHelper.JsonFull(dr.Table)
    'End Function
    Function RequestCreateObject(obj As Object) As Object
        Dim User_ID As Integer = clsLogin.ID
        Dim Website_ID As Integer = clsLogin.Agency_ID

        If Not IsNothing(obj.GetType.GetProperty("Login_ID")) Then
            obj.Login_ID = User_ID
        End If
        If Not IsNothing(obj.GetType.GetProperty("Agency_ID")) Then
            obj.Agency_ID = clsLogin.Agency_ID
        End If
        For Each prop As PropertyInfo In obj.GetType.GetProperties
            Dim propName As String = prop.Name
            Dim propType As Object = prop.PropertyType
            If Not IsNothing(Request(propName)) Then
                Dim val = StringToType(Request(propName), propType)
                prop.SetValue(obj, val, Nothing)
            End If
        Next
        Return obj
    End Function
    Function StringToType(value As String, propertyType As Type) As Object
        Dim underlyingType = Nullable.GetUnderlyingType(propertyType)
        If value = "null" Or value = "undefined" Then
            Return Nothing
        ElseIf underlyingType Is Nothing Then
            Return Convert.ChangeType(value, propertyType, CultureInfo.InvariantCulture)
        ElseIf underlyingType.FullName.IndexOf("Boolean") > -1 Then
            Return CBool(value)
        End If
        Return If([String].IsNullOrEmpty(value), Nothing, Convert.ChangeType(value, underlyingType, CultureInfo.InvariantCulture))
    End Function
    Sub FlushApiResponse(str As String)
        clsHelper.FlushResponseAPI(str)
    End Sub
    Sub FlushApiResponse(dt As DataTable)
        Dim ret As String = ""
        Dim APIContentTypt As String = Request("APIContentType")
        If Not IsNothing(APIContentTypt) Then
            APIContentTypt = APIContentTypt.Trim.ToLower
        Else
            APIContentTypt = "json"
            If IsNothing(Request("secho")) Then
                ret = clsHelper.JsonFull(dt)
            Else
                ret = clsHelper.JsonDataTablePlugin(dt, Request("secho"))
            End If
        End If

        If APIContentTypt = "xml" Then
            ret = clsHelper.XmlFull(dt)
        End If

        clsHelper.FlushResponseAPI(ret)
    End Sub
#End Region
End Class
