﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterWithMenu.master" AutoEventWireup="false" CodeFile="CustomerCheckout.aspx.vb" Inherits="CustomerCheckout" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentHolder" runat="Server">
    <!--headstart-->
    <link href="fonts/glyphicons-pro/glyphicons-pro.css" rel="stylesheet" />

    <style>
        .input-inline {
            display:inline;
            width:100px;
            height:43px;
        }

    </style>
    <!--headend-->


    <!--htmlstart-->
    <div class="row admin-form">
        <div class="col-md-2">
            <button class="button btn-primary" type="submit">New </button>

        </div>
        <div class="col-md-7">

            <div class="media clearfix">
                <div class="media-left pr30">
                    <a href="#">

                        <img alt="..." src="img/avatars/placeholder.png" style="max-width: 132px; max-height: 132px;" class="media-object mw150">
                    </a>
                </div>
                <div class="media-body admin-form">

                    <div class="section mb15">
                        <div class="option-group field">

                            <label class="option">
                                <input type="radio" name="RadioCustomer">
                                <span class="radio"></span><span runat="server">Today's customers </span>
                            </label>

                            <label class="option">
                                <input type="radio" name="RadioCustomer" checked>
                                <span class="radio"></span><span runat="server">Walk-in</span></label>
                            <label class="option">
                                <input type="radio" name="RadioCustomer">
                                <span class="radio"></span><span runat="server">Group customers </span>
                            </label>

                        </div>
                        <!-- end .option-group section -->
                    </div>


                    <div class="section">
                        <label class="field select">
                            <select name="cboRadCustomer_Input" id="cboRadCustomer_Input">
                                <option value="">Select Customer / Phone</option>

                            </select>
                            <i class="arrow"></i>
                        </label>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <span class="lbl-text" runat="server">Email</span>

                        </div>
                        <div class="col-md-6">

                            <span class="lbl-text" runat="server">Customer Since:</span>
                        </div>

                    </div>
                    <div class="row mt15">
                        <div class="col-md-6">
                            <span class="lbl-text" runat="server">Last Visited</span>
                        </div>
                        <div class="col-md-6">
                        </div>

                    </div>
                    <div class="row mt15">
                        <div class="col-md-6"><span class="lbl-text" runat="server">Birthday</span></div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="row mt15">
                        <div class="col-md-6"><span class="lbl-text" runat="server">Current Point Balance</span> </div>
                        <div class="col-md-6"><span class="lbl-text" runat="server">Membership</span> </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-3 form-horizontal">

            <div class="panel">
                <div class="panel-body">
            <div class="form-group">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Service Charges $</label>
                <div class="col-lg-4">
                    <div class="bs-component ">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" disabled="disabled">
                    </div>
                </div>
            </div>

            <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Product Charges $</label>
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" disabled="disabled">
                    </div>
                </div>
            </div>
            <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Gift Certi. Charges $</label>
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" disabled="disabled">
                    </div>
                </div>
            </div>

              <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Tax</label>
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" disabled="disabled">
                    </div>
                </div>
            </div>

            <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Tip</label>
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" >
                    </div>
                </div>
            </div>

             <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server"><b>Amount Due :</b> </label>
                <div class="col-lg-4">
                    <div class="bs-component control-label text-left">
                        <b>$0.00</b>
                    </div>
                </div>
            </div>

  <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Cash Amount</label>
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" >
                    </div>
                </div>
            </div>

   <div class="form-group mt10">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Check Amount</label>
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" >
                    </div>
                </div>
    </div>


           <div class="form-group mt10">
               <a href="#"><label for="inputStandard" class="col-lg-8 control-label text-right" runat="server"> Gift Certificate</label></a> 
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" disabled="disabled">
                    </div>
                </div>
            </div>

                    
           <div class="form-group mt10">
               <a href="#"><label for="inputStandard" class="col-lg-8 control-label text-right" runat="server">Credit Card Info</label></a> 
                <div class="col-lg-4">
                    <div class="bs-component">
                        <input type="text" id="inputStandard" class="form-control" placeholder="00" >
                    </div>
                </div>
            </div>

                    
             <div class="form-group mt10 has-success">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server"><b>Amount Paid :</b> </label>
                <div class="col-lg-4">
                    <div class="bs-component control-label text-left">
                        <b>$0.00</b>
                    </div>
                </div>
            </div>

                             
             <div class="form-group mt10 has-success">
                <label for="inputStandard" class="col-lg-8 control-label text-right" runat="server"><b>Change Due :</b> </label>
                <div class="col-lg-4">
                    <div class="bs-component control-label text-left">
                        <b>$0.00</b>
                    </div>
                </div>
            </div>
         

                   

                </div>
                </div>
        </div>


    </div>

  <div class="row admin-form">
      <div class="col-md-8">
          
                      <button class="button btn-primary" type="submit" id="btnAddService"> <i class="glyphicons glyphicons-circle_plus"></i> Service </button>
          <button class="button btn-primary" type="submit" id="btnAddPackage"> <i class="glyphicons glyphicons-circle_plus"></i> Package </button>
           <button class="button btn-primary" type="submit" id="btnAddGiftCertificate"> <i class="glyphicons glyphicons-circle_plus"></i> Gift </button>
            <button class="button btn-primary" type="submit" id="btnMembership_CustomerCheckout"> <i class="glyphicons glyphicons-circle_plus"></i> Membership </button>
          <input class="form-control input-inline"  type="text" />
              <button class="button btn-primary" type="submit" id="btnAddProduct"> <i class="glyphicons glyphicons-circle_plus"></i> Product </button>

      </div>

       <div class="col-md-4">
           <div class="row">
               <div class="col-md-5">
                   <label class="option option-primary">
                              <input type="checkbox" value="CH" name="mobileos">
                              <span class="checkbox"></span><span runat="server">Email Receipt</span>

                   </label>

               </div>
                   <div class="col-md-7"></div>

           </div>

       </div>

        
              
           

  </div>

    <!--htmlend-->

    <script type="text/javascript">
        //jsstart 

        //jsend
    </script>






</asp:Content>


