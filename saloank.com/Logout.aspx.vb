﻿
Partial Class Logout
    Inherits System.Web.UI.Page

    Private Sub Logout_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim myCookies As String() = Request.Cookies.AllKeys
        For Each cookie As String In myCookies
            clsHelper.CookieDelete(cookie)
        Next

        FormsAuthentication.SignOut()
        Response.Redirect("login.aspx")
    End Sub
End Class
