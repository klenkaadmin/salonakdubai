﻿var clsSound = (function () {
    var selector = '#SoundBtn22';
    function fnPlay() {
        $(selector)[0].play()
    }
    return {
        Play: fnPlay
    }
})();

var clsNoty = {
    info: function (text) {
        return clsNoty.view({ text: text, type: 'info', time: 5 });
    },
    error: function (text) {
        return clsNoty.view({ text: text, type: 'error', time: 5 });
    },
    success: function (text) {
        return clsNoty.view({ text: text, type: 'success', time: 5 });
    },
    view: function (o) {
        var n = noty({
            layout: 'bottomRight',
            text: o.text,
            type: o.type,
            killer: true,
            dismissQueue: true,
            closeWith: ['button', 'click']
        });
        if (o.time) {
            setTimeout(function () {
                $.noty.close(n.options.id);
            }, o.time * 1000);
        }
        return n;
    },
    confirme: function (o) {
        var n = noty({
            text: o.text,
            type: 'alert',
            layout: 'center',
            modal: true,
            killer: true,
            dismissQueue: true,
            theme: 'defaultTheme',
            buttons: [
                {
                    addClass: 'btn btn-primary btn-rad btn-sm', text: 'ok', style: 'margin-right:3px;', onClick: function ($noty) {
                        $noty.close();
                        if (typeof o.fnOk == 'function') {
                            o.fnOk();
                        }
                    }
                },
                {
                    addClass: 'btn btn-danger btn-rad btn-sm', text: 'cancel', onClick: function ($noty) {
                        $noty.close();
                        if (typeof o.fnCancel == 'function') {
                            o.fnCancel();
                        }
                    }
                }
            ]
        });
        return n;
    }
}

var clsDataTable = {
    init: function (prm) {

        var grid = $(prm.tbl).dataTable({
            "pageLength": prm.length,
            "lengthMenu": [5, 10, 25, 100, 500, 1000],
            "processing": true,
            "serverSide": true,
            "paging": true,
            "stateSave": true,
            "columns": prm.columns,
            "sAjaxSource": clsAjax.api,
            "fnServerParams": function (aoData) {
                prm.parms.map(function (item) {
                    aoData.push(item);
                });
            },
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                clsLoader.show();
                oSettings.jqXHR = $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (res) {
                        fnCallback(res);
                        clsLoader.hide();
                    }
                });
            },
            "rowCallback": prm.RowCallBack,
            "initComplete": prm.callback //function (oSettings, json) {}
        });
        return grid;
    }
}



var newGUID = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

var newID = function () {
    return (new Date()).getTime();
}

var clsForm = {
    getXmlData: function (selector) {
        var j = [{}];
        $(selector).find(".val_xml").each(function (index, ctr) {
            j[0][$(ctr).attr('data-base')] = clsForm.getValue($(ctr));
        });
        var x2js = new X2JS();
        return x2js.json2xml_str({Item:j});
    },
    getControls: function (selector, attr) {
        if (!attr) {
            attr = 'data-base';
        }
        var formdata;
        if ($(selector).find(".val*[type='file']").length > 0) {
            formdata = new FormData();
            $(selector).find(".val").each(function (index, ctr) {
                var rel = $(ctr).attr(attr);
                var type = $(ctr).attr("type");
                if (type == 'file') {
                    for (var k = 0 ; k < ctr.files.length; k++) {
                        file = ctr.files[k];
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        formdata.append(rel, file);
                    }
                }
                else {
                    formdata.append(rel, clsForm.getValue($(ctr)));
                }
            });
        }
        else {
            formdata = {};
            $(selector).find(".val[data-base]").each(function (index, ctr) {
                var rel = $(ctr).attr(attr).toLowerCase();
                var type = $(ctr).attr("type");
                if ($(ctr).is('select')) {
                    formdata[rel + '_text'] = clsForm.getText($(ctr));
                }
                formdata[rel] = clsForm.getValue($(ctr));
            });
        };
        return formdata;
    },
    getText: function (obj) {
        var type = obj.attr("type");
        if (obj.is('select')) {
            var text = obj.find('option:selected')
            if (text.length > 0) {
                if (obj.attr('multiple') && typeof text.text() == "object") {
                    return text.text().join(',');
                }
                return text.text();
            }
        }
    },
    getValue: function (obj) {
        var type = obj.attr("type");
        if (type == "checkbox" ) { return obj.is(":checked"); }
        else if ($(obj).data('select2')) {
            if (obj.attr('multiple')) {
                if (obj.select2('val')) {
                    return obj.select2('val').join(',');
                }
            }
            return obj.select2('val');
        }
        else if (obj.is('select')) {
            if (obj.attr('multiple')) {
                return obj.val().join(',')
            }
            return obj.val();
        }
        else if (obj.is(':input')) return obj.val();
        else if (obj[0].val != undefined) return obj[0].val();
        else if (!type) { return obj.html(); }
    }, 
    validate: function (selector) {
        var form = $("form:first");
        if (selector) {
            if (typeof selector === 'string') {
                form = $(selector);
            }
            else {
                form = $(selector);
            }
        }
        form.validationEngine({
            scroll: true,
            "promptPosition": GetPosition()

        });
        function GetPosition() {
            if (clsUrl.getParam('view')) {
                return 'bottomLeft';
            }
            else {
                return 'topLeft';
            }
        }
        if (form.validationEngine('validate')) {
            return true;
        }
        else {
            form.validationEngine('detach');
            return false;
        }
    },
    setControl: function (FormSelector, attr, json) {
        for (var key in json) {
            var elem = $(FormSelector).find("[" + attr + "='" + key + "']");
            var type = elem.attr("type");
            if (type == "checkbox" || type == "radio") {
                if (json[key] == 1) {
                    elem.attr('checked', 'checked');
                }
            }
            else if (type == "text" || type == "password" || elem.is("textarea") ) {
                elem.val(json[key]);
            }
            else if (elem.is("select")) {
                if (elem.data('select2')) {
                    elem.select2('val', json[key]);
                }
                else {
                    elem.find('option').each(function (indx, opt) {
                        if (typeof json[key] === 'string' || typeof json[key] === 'integer') {
                            if ($(opt).val() == json[key]) {
                                $(opt).attr('selected', 'selected');
                            }
                        }
                        else {
                            json[key].map(function (v) {
                                if ($(opt).val() == v) {
                                    $(opt).attr('selected', 'selected');
                                }
                            });
                        }
                    });
                }
            }
            else {
                elem.html(json[key]);
            }
        }
       
    }
}

var clsJson = {
    replaceBadChars: function (v) {
        v = v.toString();
        if (v == true || v == false) return v;
        var ret = v.replace(/\'/g, "\\'");
        ret = ret.replace(/\n/g, "\\n");
        ret = ret.replace(/\\/g, "\\\\");
        var reg = /"/g;
        var newstr = '\\"';
        ret = ret.replace(reg, newstr);
        ret = ret.replace(/</g, '< ');
        ret = ret.replace(/<  /g, '< ');
        return ret;
    }
}

var clsFile = {
    getName: function (file) {
        var f = file.replace(/^.*(\\|\/|\:)/, '');
        return f.replace('.' + clsFile.getExtension(file), '');
    },
    getExtension: function (file) {
        return file.replace(/^.*?\.([a-zA-Z0-9]+)$/, "$1");
    },
    download: function (path) {
        /* download functio = download_file */
        $('#down_fram').remove();
        $("body").append("<iframe id='down_fram' src='" + clsAjax.api + "?action=download_file&path=" + path + "' style='display: none;'></iframe>");
    }
}

var clsLoader = {
    show: function () {
        NProgress.start();
    },
    hide: function () {
        NProgress.done();
    },
    set: function (v) {
        NProgress.set(parseFloat(v));
    }
}

var clsPage = (function () {
    var HolderID = 'hid_' + (new Date()).getTime();

    var LoaderDiv = '#preloader,#preloader div';

    function fnLoaderShow() {
        //$(LoaderDiv).show();
        clsLoader.show();
    }
    function fnLoaderHide() {
        //$(LoaderDiv).hide();
        clsLoader.hide();
    }
    function fnCheckOffline() {
        if (!window.navigator.onLine) {
            window.location.hash = '/error.aspx';
            return true;
        }
        else {
            return false;
        }
    }

    function fnLoad(options) {
        clsPage.LoaderShow();
        if (options['PageParams']) {
            window.PageParams = options['PageParams'];
        }

        var def = {
            url: '',
            targetdiv: '#content',
            iframe: false,
            addclass: '',
            id: 'holder_' + (new Date()).getTime(),
            calljs: ''
        }
        options = $.extend(def, options);


        if (options.reload) {
            $("[data-url='" + options.url + "']").remove();
        };
        if ($("[data-url='" + options.url + "']").length == 0) {
            if (options.iframe) {
                if (options.url.indexOf('?') == -1) {
                    options.url = options.url + '?hid=' + options.id;
                }
                else {
                    options.url = options.url + '&hid=' + options.id;
                }
                options.url = options.url + '&view=iframe';
                $(options.targetdiv).append("<div data-url='" + options.url + "' id='" + options.id + "' class='mainnav'><iframe class='" + options.addclass + "'  src='" + options.url + "?dlgid=" + options.id + "' width='100%' height='100%'  marginWidth='0' marginHeight='0'  frameborder='0'/></div>")
            }
            else {
                options.url = '/_loader.aspx?hid=' + HolderID + '&url=' + encodeURIComponent(options.url);
                $(options.targetdiv).append("<div data-url='" + options.url + "' id='" + options.id + "' class='mainnav " + options.addclass + "'></div>")
                loadPage(options);
            }
        }
        $(".mainnav").hide(function () {
            $('#' + options.id).show();
        });

        function loadPage(options) {
           return clsAjax.ajax({
                url: options.url,
                type: 'GET',
                cache: true,
                dataType: 'html',
                success: function (html) {
                    $('#content').html(html).promise().done(function () {
                        if (options['success']) {
                            options.success(html);
                        }
                        if (options.calljs != '') eval(options.calljs);
                        clsPage.LoaderHide();

                        $(".mainnav").hide(function () {
                            $('#' + options.id).show();
                        });
                    });
                }
            });
        }
    }
    return {
        CheckOffline: fnCheckOffline,
        Load: fnLoad,
        LoaderShow: fnLoaderShow,
        LoaderHide: fnLoaderHide
    }
})()

var clsAjax = {
    api: '/API/',
    ajax: function (params) {
        clsLoader.show();
        if (typeof params.data === 'function') {
            params.data = params.data();
        }
        var vals = {
            type: 'POST',
            url: clsAjax.api ,
            data: {},
            dataType: 'json',
            cache: false,
            contentType: 'application/x-www-form-urlencoded',
            async: true,
            success: function (result) {
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Error:" + errorThrown);
            }
        }
        params = $.extend(vals, params);
        $.ajax({
            type: params.type,
            url: params.url,
            data: params.data,
            dataType: params.dataType,
            contentType: params.contentType,
            async: params.async,
            cache: params.cache,
            success: function (result) {
                clsLoader.hide();
                params.success(result);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                params.error(XMLHttpRequest, textStatus, errorThrown);
                clsLoader.hide();
            }
        });

        //$.post(params.url, params.data, function (result) {
        //    clsLoader.hide();
        //    params.success(result);
        //}, "json").error(function (XMLHttpRequest, textStatus, errorThrown) {
        //    params.error(XMLHttpRequest, textStatus, errorThrown);
        //    clsLoader.hide();
        //});
    },
    ajaxFile: function (params) {
        clsLoader.show();
        var vals = {
            url: clsAjax.api,
            prm: {},
            dataType: 'json',
            success: function (result) {
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Error:" + errorThrown);
            }
        }
        params = $.extend(vals, params);

        $.ajax({
            type: 'POST',
            url: params.url,
            cache: false,
            contentType: false,
            processData: false,
            dataType: params.dataType,
            data: params.prm,
            success: function (result) {
                clsLoader.hide();
                params.success(result);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                params.error(XMLHttpRequest, textStatus, errorThrown);
                clsLoader.hide();
            }
        });
    }
}

var clsUrl = {
    encode: function (url) {
        return encodeURI(url);
    },
    decode: function (url) {
        return decodeURI(url);
    },
    getPageName: function () {
        clsUrl.getPageNameByUrl(window.location.href);
    },
    getPageNameByUrl: function (url) {
        return url.replace(/^.*(\\|\/|\:)/, '').split(".aspx")[0];
    },
    getParam: function (param) {
        return clsUrl.getParamByUrl(window.location.href, param);
    },
    getParamByUrl: function (url, param) {
        param = decodeURIComponent((url.split("#")[0].match(RegExp("[?|&]" + param + '=(.+?)(&|$)', 'i')) || [, null])[1]);
        if (param == "null") {
            return '';
        }
        else {
            return param;
        }
    },
    isUrl: function (url) {
        if (url.indexOf('.aspx') > -1) { return true }
        else { return false }
    }
}

var clsObject = {
    isObject: function (obj) {
        if (obj.constructor === Object) { return true; }
        else return false;
    }
}

var clsModal = {
    close: function (id) {
        $('#Modal_' + id).modal('hide');
        setTimeout(function () {
            $('#Modal_' + id).remove();
        }, 1000);
    },
    OpenIframe: function (j) {
        if(!j.size){
            j.size = 'modal-lg';
        }
       return clsModal.init({
            iframe: {
                src: j.url,
                style:j.style
            },
            modal: {
                size: j.size
            }
        });
    },
    init: function (obj) {
        var iv = {
            id: newID(),
            json:{},
            title: '',
            header: null,
            footer: null,
            button: [],
            html: '',
            iframe:null,
            modal: {
                show: true,
                size: 'modal-lg'
            }
        }
        if (obj.title) {
            obj.header = 1;
        }
        obj = $.extend(iv, obj);
        if (obj.iframe) {
            if (obj.iframe.src) {
                if (obj.iframe.src.indexOf('?') == -1) { obj.iframe.src += '?' }
            }
            obj.iframe.src += '&iframeID=' + obj.id;
        }

        var source = $("#module_temp").html();
        var template = Handlebars.compile(source);
        var rendered = template(obj);
        $('body').append(rendered);

        var modal = $('#Modal_' + obj.id).modal({
            show: obj.modal.show,
            keyboard:true
        });
        modal.on('hide.bs.modal', function () {
            $(this).remove();
        });
        modal.on('hidden.bs.modal', function () {
            $(this).remove();
        });
        return modal;
    }
}
