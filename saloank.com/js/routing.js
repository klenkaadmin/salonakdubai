'use strict';
(function($) {
    routie({
        '': home,
        'home': home,
        'category/:id?/:title?': category,
        '*': all
    });


    function home() {
        clsPage.Load({
            url: 'Home.aspx',
            success: function (html) {
                $('#SiteHolder').html(html);
            }
        });
    }

    function category(id, title) {

    }


    function all() {
        var hash = window.location.hash.split('#')[1];
        clsPage.LoaderShow();
        clsPage.Load({
            url: hash
        });
    }
})(jQuery);

